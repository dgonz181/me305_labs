'''@ file Fibonnacci Sequence.py'''

import math
A=(1+math.sqrt(5))/2
B=(1-math.sqrt(5))/2

def fib(idx):

    '''
    @brief This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired Fibonacci number
    @source for math expression https://math.hmc.edu/funfacts/fibonacci-number-formula/
    '''
        

    idx= round((A**idx-B**idx)/(math.sqrt(5)))

    return idx



if __name__ == '__main__':

    

    idx= float (input(' Enter an index starting from 1 and up'))

    if idx<1:'ensures that the program runs until a positive integer is inputed.
        
        while idx<1:
            
            
            idx= float (input(' Enter an index starting from 1 and up'))


    idx=idx

    print ('Fibonacci number at '
           'index {:} is {:}.'.format(idx,fib(idx)))

